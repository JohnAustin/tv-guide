import re

def time_convert(x):
    """This function will take any amount of minutes
    and convert it into the standered HH:MM
    e.g Passing the converter 60 will return 100
    Passing the converter 90 will return 130
    It will only return whole numbers nor insert a colon
    """
    if x < 60:
        return x
    elif x == 60:
        x = 100
    elif x > 60 and  x < 120:
        x = x - 60 + 100
    elif x == 120:
        x = 200
    elif x > 120 and x < 180:
        x = x - 120 + 200
    elif x == 180:
        x = 300
    elif x > 180 and x < 240:
        x = x - 180 + 300
    elif x == 240:
        x = 400
    elif x > 240 and x < 300:
        x = x - 240 + 400
    elif x == 300:
        x = 500
    elif x > 300 and x < 360:
        x = x - 300 + 500
    elif x == 360:
        x = 600
    elif x > 360 and x < 420:
        x = x - 360 + 600
    elif x == 420:
        x = 700
    elif x > 420 and x < 480:
        x = x - 420 + 700
    elif x == 480:
        x = 800
    elif x > 480 and x < 540:
        x = x - 480 + 700
    elif x == 540:
        x = 900
    return x

check_num = 0
def deconstruct(x):
    """This function simple deconstructs a yyyymmddhhmmss
    format and return only the HHMM portion.
    """
    keep_last_six = str(x)[-6:]
    remove_seconds = int(keep_last_six)/100
    hhmm = (int(remove_seconds))
    return hhmm

def check_overage(x):
    """This checks if any time addtion bring it over the 60 minute mark
    e.g if a previous function returned 63 minutes this function will 
    correct this fault
    """
    keep_last_two = str(x)[-2:]
    back_to_int = int(keep_last_two)
    if back_to_int >= 60:
        corrected_time = time_convert(back_to_int)
        new_time = (corrected_time - back_to_int + x) * 100
        return new_time
    else:
        new_time = -1
        return new_time

def validate(x):
    """This function validates and returns a proper yyyymmddhhmmss int
    this is used when adding time to yyyymmddhhmmss in order to ensure
    it makes logical sense
    """
    validate = str(x)
    split_ymd = validate[:8]
    hhmm = deconstruct(validate)
    corrected_time = check_overage(hhmm)
    if corrected_time > 0:
        if len(str(corrected_time)) != 6:
            finalized_time = str(split_ymd)+"0"+str(corrected_time)
            return finalized_time
        else:
            finalized_time = str(split_ymd)+str(corrected_time)
            return finalized_time
    else:
        finalized_time = validate#Left over junk line?#
        return validate