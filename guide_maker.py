import bs4 as bs
import urllib.request as rs
import re
from time_converter import validate
from time_converter import time_convert

# sites = sites.txt
# guide = guide.xml
station = []
# ##########Print Header##########
def print_header():
    with open("guide.xml","w") as file:
        file.write(str('<?xml version="1.0" encoding="UTF-8"?>\n<tv generator-info-name="Simple Python XMLTV Generator" generator-info-url="http://www.gitlab.com">\n'))

###########Load Sites Wanted###########
def sites():
    urls = []
    with open("sites.txt","r") as sites:
        for url in sites:
            urls.append(url)
    return urls

############Grab Site Bodys############
def load_sites():
    body = []
    for item in sites():
        channel_prep = re.sub("https://www.tvpassport.com/tv-listings/stations/","",item)
        channel_extract = re.sub(r'/\d*$',"",channel_prep)
        rm_newline = re.sub('\n',"",channel_extract)
        channel_list = rm_newline.split("-")
        city = channel_list[2].capitalize()
        parent = channel_list[0].upper()
        channel_name = channel_list[1].upper()
        state_province = channel_list[3].upper()
        full = str("{} ({}) {}, {}").format(parent,channel_name,city,state_province)
        with open("guide.xml","a") as file:
            file.write(str('  <channel id="{}">\n    <display-name lang="en">{}</display-name>\n    <icon src="" />\n    <url>https://www.tvpassport.com</url>\n  </channel>\n').format(full,full))
        source = rs.urlopen(item).read()
        soup = bs.BeautifulSoup(source,'html.parser')
        item = soup.body.find_all('div' , class_="list-group-item")
        body.append(item)
        station.append(full)
    return body

info = load_sites()

def print_programme():
    n = 0
    for programme in info:
        i = 0
        while i < len(programme):
            nd = programme[i].attrs
            full = station[n]
            start = str(re.sub(r"([-: ])","",nd.get('data-st')))
            start_format = str(start + " -0400")
            duration = int(nd.get('data-duration'))
            converted_minutes = time_convert(duration)
            start_and_converted_minutes = int(start) + (converted_minutes*100)
            stop_time = validate(start_and_converted_minutes)
            stop_format = str(stop_time + " -0400")
            description = nd.get('data-description')
            showname = str(re.sub('-',"",nd.get('data-showtitle')).rstrip())
            with open("guide.xml", "a") as file:
                file.write(str('\n  <programme start="{}" stop="{}" channel="{}">\n    <title lang="en">{}</title>\n    <desc lang="en">{}(n)</desc>\n  </programme>').format(start_format,stop_format,full,showname,description))
            i += 1
        n += 1

def ending():
    with open("guide.xml", "a") as file:
        file.write('\n</tv>')

def make_guide():
    print_header()
    load_sites()
    print_programme()
    ending()

make_guide()